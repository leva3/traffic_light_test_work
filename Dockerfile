FROM python:3.10

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /traffic_light

COPY requirements.txt /traffic_light/
ADD wsgi-entrypoint.sh /traffic_light/

RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . /traffic_light/