import requests as req

import yandex_weather_api


from dotenv import load_dotenv
import os

load_dotenv('./.env')
YANDEX_KEY = os.environ.get('YANDEX_KEY')


def get_weather(name: str, data: dict) -> dict:
    # Преобразовываем название города в нижний регистр
    name = name.lower()
    # По названию города получаем в виде массива его координаты из словаря
    coordinates = data.get(name)
    # Если такой город есть в списке то получим его координаты, иначе None
    if coordinates != None:
        # Выносим координаты в отдельные переменные для удобства и читабельности
        lat=float(coordinates[0])
        lon=float(coordinates[1])
        # Делаем запрос на яндекс
        res = yandex_weather_api.get(req, YANDEX_KEY, lat=lat, lon=lon, rate='forecast')
        # Получаем нужные нам данные в ответе
        temp = res['fact']['temp']
        wind_speed = res['fact']['wind_speed']
        pressure_mm = res['fact']['pressure_mm']

        # Формируем из этих данных словарь
        result = {
            'temp': temp,
            'wind_speed': wind_speed,
            'pressure_mm': pressure_mm,
            'status': True
        }
        # Возвращаем ответ
        return result
    else:
        return {
            "message": "Такого города нет в списке",
            'status': False
        }
