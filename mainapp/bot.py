import telebot
from telebot import types

from dotenv import load_dotenv
import os


from data import data as cities
from api_test import get_weather

load_dotenv('./.env')
TOKEN = os.environ.get('TELEGRAM_KEY')

check_weather = 'Узнать погоду'

start_keyboard = types.ReplyKeyboardMarkup(True)
start_keyboard.add(check_weather)


bot = telebot.TeleBot(TOKEN)

@bot.message_handler(commands=["start"])
def start_bot(message):
    start_text = "Вас приветствует погодный бот работающий на основе данных от Яндекс.Погода\n\n\
        Введите названия города для получения информации о погоде в нем на сегодня"
    bot.send_message(
        message.chat.id, start_text,
        reply_markup=start_keyboard,
    )

@bot.message_handler(content_types=['text'])
def send_info_weather(message):
    if message.text == check_weather:
        bot.send_message(
            message.chat.id,
            'Введите название города'
        )
    else:
        city_name = message.text
        weather_result = get_weather(
            name=city_name,
            data=cities
        )
        if weather_result.get('status'):
            msg = f"Погода в городе {city_name}\n\n\
                    Температура: {weather_result.get('temp')} градусов по цельсию\n\
                    Скорость ветра: {weather_result.get('wind_speed')} метров в секунду\n\
                    Атмосферное давление: {weather_result.get('pressure_mm')} мм рт. ст."

            bot.send_message(
                message.chat.id,
                msg
            )
        else:
            bot.send_message(
                message.chat.id,
                'Такого города нет в списке, попробуйте еще раз'
            )


if __name__ == "__main__":
    bot.polling(none_stop=True)
