from django.urls import path

from rest_framework.routers import DefaultRouter as DR

from mainapp.views import(
    WeatherAPIView,
)

router = DR()

urlpatterns = [
    path('weather/', WeatherAPIView.as_view(), name='weather_api'),
]

urlpatterns += router.urls
