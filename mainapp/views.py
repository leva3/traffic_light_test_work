from rest_framework.views import APIView

from rest_framework.response import Response

from django.core.cache import cache

from mainapp.api_test import get_weather

from mainapp.data import data as cities


class WeatherAPIView(APIView):
    # Создаем GET запрос для получения данных о погоде
    def get(self, request):
        # Получаем название города из query_params
        city = request.query_params.get('city', None)
        # Проверяем было ли передано название города
        if city != None:
            # Если передано то пытаемся получить его из кеша redis
            weather_cache = cache.get(city)
            # Если в redis кеше был наш город то записываем значение в глобальную переменную
            if weather_cache:
                weather = weather_cache
            else:
                # Если в кеше redis нет нашего города то делаем запрос на yandex weather api
                print(f'Делаем запрос в Yandex по городу {city}', '\n\n\n')
                weather = get_weather(
                    name=city,
                    data=cities
                )
                # После получения ответа от яндекса сохраняем в redis кеш с ключем название города
                # значение погода в этом городе
                # и на какое время хотим сохранить в кеш 60 секунд * 30 = 30 минут
                cache.set(city, weather, 60*30)
            # Отдаем ответ пользователю
            return Response(
                weather
            )
        # Если название города не было передано то возвращаем сообщение об этом
        else: 
            return Response(
                {
                    'message': 'Enter city name in query parameters with key: city'
                }
            )
